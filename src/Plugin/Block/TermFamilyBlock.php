<?php

namespace Drupal\term_family\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\term_family\TermFamilyInterface;

/**
 * Provides a 'TermFamilyBlock' block.
 *
 * @Block(
 *  id = "term_family_block",
 *  admin_label = @Translation("Term Family"),
 * )
 */
class TermFamilyBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\term_family\TermFamilyInterface definition.
   *
   * @var \Drupal\term_family\TermFamilyInterface
   */
  protected $termFamily;

  /**
   * Constructs a new TermFamilyBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\term_family\TermFamilyInterface $term_family
   *   Term family service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TermFamilyInterface $term_family
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->termFamily = $term_family;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('term_family')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'term_context' => TermFamilyInterface::CONTEXT_URL,
      'term' => NULL,
      'term_display' => TermFamilyInterface::DISPLAY_LABEL,
      'set_link' => 1,
      'view_mode' => 'full',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['term_context'] = [
      '#type' => 'radios',
      '#title' => $this->t('Term context'),
      '#options' => [
        TermFamilyInterface::CONTEXT_URL => $this->t('Get the Taxonomy term ID form URL'),
        TermFamilyInterface::CONTEXT_FIXED => $this->t('Fixed'),
      ],
      '#default_value' => $this->configuration['term_context'],
    ];
    $term = NULL;
    if ($this->configuration['term'] !== NULL) {
      $term = $this->termFamily->termLoad((int) $this->configuration['term']);
    }
    $form['term'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#title' => $this->t('Term'),
      '#description' => $this->t('The term that will get its family.'),
      '#default_value' => $term,
      // @todo fix states
      '#states' => [
        'visible' => [
          ':input[name="term_context"]' => ['value' => (string) TermFamilyInterface::CONTEXT_FIXED],
        ],
        'required' => [
          ':input[name="term_context"]' => ['value' => (string) TermFamilyInterface::CONTEXT_FIXED],
        ],
      ],
    ];
    $form['term_display'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display'),
      '#options' => [
        TermFamilyInterface::DISPLAY_LABEL => $this->t('Label'),
        TermFamilyInterface::DISPLAY_RENDERED_ENTITY => $this->t('Rendered entity'),
      ],
      '#description' => $this->t('The view mode that will be used to display the terms.'),
      '#default_value' => $this->configuration['term_display'],
    ];
    $form['set_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Link to the Taxonomy term'),
      '#default_value' => $this->configuration['set_link'],
      // @todo fix states
      '#states' => [
        'visible' => [
          ':input[name="term_display"]' => ['value' => TermFamilyInterface::DISPLAY_LABEL],
        ],
        'required' => [
          ':input[name="term_display"]' => ['value' => TermFamilyInterface::DISPLAY_LABEL],
        ],
      ],
    ];
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#options' => $this->termFamily->getTermViewModes(),
      '#description' => $this->t('The view mode that will be used to display the terms.'),
      '#default_value' => $this->configuration['view_mode'],
      // @todo fix states
      '#states' => [
        'visible' => [
          ':input[name="term_display"]' => ['value' => TermFamilyInterface::DISPLAY_RENDERED_ENTITY],
        ],
        'required' => [
          ':input[name="term_display"]' => ['value' => TermFamilyInterface::DISPLAY_RENDERED_ENTITY],
        ],
      ],
    ];
    // @todo add maximum nested levels
    // @todo add reduce duplicates among multiple parents

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['term_context'] = $form_state->getValue('term_context');
    $this->configuration['term'] = $form_state->getValue('term');
    $this->configuration['term_display'] = $form_state->getValue('term_display');
    $this->configuration['set_link'] = $form_state->getValue('set_link');
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
  }

  /**
   * Returns the term from the manual configuration or the URL context.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The term to get the family from.
   */
  private function getTermFromConfiguration() {
    $termId = NULL;
    switch ($this->configuration['term_context']) {
      case TermFamilyInterface::CONTEXT_URL:
        $termId = $this->termFamily->getTermIdFromUrl();
        break;

      case TermFamilyInterface::CONTEXT_FIXED:
        $termId = $this->configuration['term'];
        break;
    }
    return $this->termFamily->termLoad((int) $termId);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $term = $this->getTermFromConfiguration();
    $termFamily = $this->termFamily->familyDisplay($term->id(), $this->configuration);
    if (!empty($termFamily)) {
      $build = [
        '#theme' => 'term_family',
        '#term' => $term,
        '#family' => $termFamily,
      ];
    }
    $build['#cache'] = [
      'contexts' => [
        'languages:language_interface',
        'url.path',
      ],
    ];
    return $build;
  }

}

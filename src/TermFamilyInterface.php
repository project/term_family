<?php

namespace Drupal\term_family;

/**
 * Interface TermFamilyInterface.
 */
interface TermFamilyInterface {

  const DISPLAY_LABEL = 0;

  const DISPLAY_RENDERED_ENTITY = 1;

  const CONTEXT_URL = 0;

  const CONTEXT_FIXED = 1;

  /**
   * Returns a list of terms entities related to a term.
   *
   * @param int $term_id
   *   Term id.
   *
   * @return array
   *   List of Term entities as a family tree.
   */
  public function getFamily($term_id);

  /**
   * Fetches the current term id from the URL.
   *
   * @return int
   *   Taxonomy term id.
   */
  public function getTermIdFromUrl();

  /**
   * Returns a list of available view modes for terms.
   *
   * @return array
   *   List of view modes.
   */
  public function getTermViewModes();

  /**
   * Shorthand that loads a Term entity.
   *
   * @param int $term_id
   *   Term id.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   Taxonomy term.
   */
  public function termLoad($term_id);

  /**
   * Returns a render array for a list of terms including the active state.
   *
   * @param int $term_id
   *   The term id that will be used for the family relationship.
   * @param array $configuration
   *   The block configuration.
   *
   * @return array
   *   Render array for a list of terms.
   */
  public function familyDisplay($term_id, array $configuration);

}

<?php

namespace Drupal\term_family;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;

/**
 * Class TermFamily.
 */
class TermFamily implements TermFamilyInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityDisplayRepositoryInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The term to get the family from.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  private $term;

  /**
   * Constructs a new TermFamily object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, CurrentRouteMatch $current_route_match) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function getFamily($term_id) {
    $result = [];
    $term = $this->termLoad($term_id);
    $termParents = $this->getParents($term);
    // @todo set as recursive to get multiple levels
    // If the term has no parents, assume that it is a potential parent.
    if (empty($termParents)) {
      // Get all its children first.
      $termChildren = $this->getChildren($term);
      // If there are no children, do not display a single parent.
      if (!empty($termChildren)) {
        $result[$term_id]['term'] = $term;
        $result[$term_id]['children'] = $termChildren;
        // @todo for each children, get the parents
      }
      // Otherwise get all the term parents children and group them by parent.
    }
    else {
      foreach ($termParents as $parentTid => $parentTerm) {
        $result[$parentTid]['term'] = $parentTerm;
        $result[$parentTid]['children'] = $this->getChildren($parentTerm);
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getTermIdFromUrl() {
    $term = $this->currentRouteMatch->getParameter('taxonomy_term');
    if ($term instanceof TermInterface) {
      $result = $term->id();
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getTermViewModes() {
    return $this->entityDisplayRepository->getViewModeOptions('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public function termLoad($term_id) {
    // @todo caching
    $term = NULL;
    try {
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($term_id);
    }
    catch (InvalidPluginDefinitionException $exception) {
      \Drupal::logger('term_family')->error($exception->getMessage());
    }
    return $term;
  }

  /**
   * {@inheritdoc}
   */
  public function familyDisplay($term_id, array $configuration) {
    $build = [];
    $items = [];
    $family = $this->getFamily($term_id);
    if (!empty($family)) {
      // @todo set as recursive to get multiple levels
      foreach ($family as $parentTid => $parent) {
        $activeStateClass = $parentTid == $term_id ? 'active' : 'inactive';
        $items[$parentTid] = [
          '#markup' => $this->termDisplay($parent['term'], $configuration),
          '#wrapper_attributes' => [
            'class' => ['term_family__parent', $activeStateClass],
          ],
        ];
        foreach ($parent['children'] as $childTid => $child) {
          $activeStateClass = $childTid == $term_id ? 'active' : 'inactive';
          $items[$parentTid]['children'][] = [
            '#markup' => $this->termDisplay($child, $configuration),
            '#wrapper_attributes' => [
              'class' => ['term_family__child', $activeStateClass],
            ],
          ];
        }
      }
      $build['family-tree'] = [
        '#theme' => 'item_list',
        '#items' => $items,
        '#type' => 'ul',
        '#attributes' => ['class' => 'term_family'],
      ];
    }
    return $build;
  }

  /**
   * Renders a term depending on the configuration.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to render.
   * @param array $configuration
   *   The display configuration.
   *
   * @return string
   *   The term markup.
   */
  private function termDisplay(TermInterface $term, array $configuration) {
    $renderer = \Drupal::service('renderer');
    $result = [];
    switch ($configuration['term_display']) {
      case TermFamilyInterface::DISPLAY_LABEL:
        if ($configuration['set_link']) {
          $url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()]);
          $link = Link::fromTextAndUrl($term->label(), $url);
          $build = $link->toRenderable();
          $result = $renderer->renderRoot($build);
        }
        else {
          $result = $term->label();
        }
        break;

      case TermFamilyInterface::DISPLAY_RENDERED_ENTITY:
        $viewBuilder = $this->entityTypeManager->getViewBuilder('taxonomy_term');
        $build = $viewBuilder->view($term, $configuration['view_mode']);
        $result = $renderer->renderRoot($build);
        break;
    }
    return $result;
  }

  /**
   * Returns the tree of the Term bundle.
   *
   * @todo improve getFamily with getTree or remove.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The Term to fetch the tree from.
   *
   * @return array
   *   List of Terms.
   */
  private function getTree(TermInterface $term) {
    $result = [];
    try {
      /** @var \Drupal\taxonomy\TermStorageInterface $termStorage */
      $termStorage = $this->entityTypeManager->getStorage("taxonomy_term");
      $result = $termStorage->loadTree($term->bundle());
    }
    catch (InvalidPluginDefinitionException $exception) {
      \Drupal::logger('term_family')->error($exception->getMessage());
    }
    return $result;
  }

  /**
   * Returns the parents of a Term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The Term to fetch the parents from.
   *
   * @return array
   *   List of Terms.
   */
  private function getParents(TermInterface $term) {
    $result = [];
    try {
      /** @var \Drupal\taxonomy\TermStorageInterface $termStorage */
      $termStorage = $this->entityTypeManager->getStorage("taxonomy_term");
      $parentTerms = $termStorage->loadAllParents($term->id());
      foreach ($parentTerms as $parentTerm) {
        if ($parentTerm->id() !== $term->id()) {
          $result[$parentTerm->id()] = $parentTerm;
        }
      }
    }
    catch (InvalidPluginDefinitionException $exception) {
      \Drupal::logger('term_family')->error($exception->getMessage());
    }
    return $result;
  }

  /**
   * Returns the children of a Term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The Term to fetch the children from.
   *
   * @return array
   *   List of Terms.
   */
  private function getChildren(TermInterface $term) {
    $result = [];
    try {
      /** @var \Drupal\taxonomy\TermStorageInterface $termStorage */
      $termStorage = $this->entityTypeManager->getStorage("taxonomy_term");
      $childTerms = $termStorage->loadChildren($term->id());
      foreach ($childTerms as $childTerm) {
        if ($childTerm->id() !== $term->id()) {
          $result[$childTerm->id()] = $childTerm;
        }
      }
    }
    catch (InvalidPluginDefinitionException $exception) {
      \Drupal::logger('term_family')->error($exception->getMessage());
    }
    return $result;
  }

}
